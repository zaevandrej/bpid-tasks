def get_frequencies_table(text: str, only_alpha: bool=True) -> dict:
    if only_alpha:
        letters = list(filter(
            lambda c: c.isalpha(),
            text.lower()))
    else:
        letters = list(text.lower())

    table = {}

    for letter in letters:
        if letter in table:
            table[letter] += 1
        else:
            table[letter] = 1

    return table

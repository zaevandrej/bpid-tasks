import os
import base64


def generate_key(message_bytes: bytes) -> bytes:
    return os.urandom(len(message_bytes))


def otp(message_bytes: bytes, key: bytes) -> bytes:
    if len(message_bytes) != len(key):
        raise ValueError('Key must have the same length as message')

    result = bytes([pair[0] ^ pair[1] for pair in zip(message_bytes, key)])

    return result


def encode(message: str, key: str=None) -> (str, str):
    message_bytes = bytes(message, 'utf-8')

    if key is None:
        key_bytes = generate_key(message_bytes)
    else:
        key_bytes = bytes(message, 'utf-8')

    encoded_bytes = otp(message_bytes, key_bytes)

    encoded = base64.b64encode(encoded_bytes)

    return encoded.decode('ascii'), base64.b64encode(key_bytes).decode('ascii')


def decode(message: str, key: str) -> str:
    message_bytes = base64.b64decode(message)
    key_bytes = base64.b64decode(key)

    result = otp(message_bytes, key_bytes)

    return result.decode('utf-8')

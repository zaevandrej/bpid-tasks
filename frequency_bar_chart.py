import numpy as np
import matplotlib.pyplot as plt


def _create_labels(axes, rects):
    for rect in rects:
        height = rect.get_height()
        axes.text(rect.get_x() + rect.get_width(), 1.05 * height,
                  '%.2f' % height,
                  ha='center', va='bottom', size='xx-small')


def _to_percents(table: dict) -> dict:
    total_count = sum(table.values())

    return {pair[0]: pair[1] / total_count * 100 for pair in table.items()}


def show(table: dict) -> None:
    entries_count = len(table)

    percentage = _to_percents(table)

    ordered_table = sorted(percentage.items(), key=lambda pair: (pair[1], pair[0]), reverse=True)

    frequencies = list(map(lambda p: p[1], ordered_table))
    characters = list(map(lambda p: p[0], ordered_table))

    ind = np.arange(entries_count)
    width = 0.35

    figure, axes = plt.subplots()
    rects = axes.bar(ind, frequencies, width, color='b')

    figure.canvas.set_window_title('Frequencies')

    axes.set_ylabel('Frequency, %')
    axes.set_title('Frequencies')
    axes.set_xticks(ind - width / 2)
    axes.set_xticklabels(characters)

    _create_labels(axes, rects)

    plt.show()

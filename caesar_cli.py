import getopt
import sys
import caesar

MODE_ENCRYPT = 1
MODE_DECRYPT = 2


def main(argv: list) -> None:
    mode = 0
    message = ''
    shift = 0

    try:
        opts, _ = getopt.getopt(argv, 'hedi:k:')
    except getopt.GetoptError:
        print('usage: python caesar_cli.py (-e|-d) -i <input> -k <shift>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('usage: python caesar_cli.py (-e|-d) -i <input> -k <shift>')
            sys.exit()
        elif opt == '-e':
            if mode != 0:
                print('Must be used either -e or -d options')
                sys.exit(2)

            mode = MODE_ENCRYPT
        elif opt == '-d':
            if mode != 0:
                print('Must be used either -e or -d options')
                sys.exit(2)

            mode = MODE_DECRYPT
        elif opt == '-i':
            message = arg
        elif opt == '-k':
            shift = int(arg)

    if mode == MODE_ENCRYPT:
        encrypted = caesar.encrypt(message, shift)

        print(encrypted)
    elif mode == MODE_DECRYPT:
        decrypted = caesar.decrypt(message, shift)

        print(decrypted)
    else:
        print('Must be used either -e or -d options')
        sys.exit(2)

if __name__ == '__main__':
    main(sys.argv[1:])

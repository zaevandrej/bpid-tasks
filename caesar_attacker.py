import frequency
import caesar

TOP_LIMIT = 4


def _get_message_most_frequent(message):
    message_frequencies = frequency.get_frequencies_table(message)

    ordered = sorted(message_frequencies.items(), key=lambda pair: (pair[1], pair[0]), reverse=True)

    top = ordered[:TOP_LIMIT]

    return list(map(lambda p: p[0], top))


def attack(encrypted_message: str, real_most_frequent: list) -> str:
    if len(real_most_frequent) != TOP_LIMIT:
        raise ValueError('List with length {0} expected'.format(TOP_LIMIT))

    message_most_frequent = _get_message_most_frequent(encrypted_message)

    shifts = {}

    for r in real_most_frequent:
        for m in message_most_frequent:
            r_code = ord(r)
            m_code = ord(m)

            if m_code > r_code:
                shift = m_code - r_code
            else:
                shift = m_code - r_code + caesar.ALPHABET_SIZE

            if shift in shifts:
                shifts[shift] += 1
            else:
                shifts[shift] = 1

    possible_shifts = filter(
        lambda shift_info: shift_info[1] > 1,
        sorted(shifts.items(), key=lambda pair: (pair[1], pair[0]), reverse=True))

    for (shift, _) in possible_shifts:
        print(caesar.decrypt(encrypted_message, shift))
        print('Shift: {0}\n'.format(shift))

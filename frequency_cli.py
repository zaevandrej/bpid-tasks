import sys
import getopt
import frequency
import frequency_bar_chart


def print_table(table: dict, output_limit: int) -> None:
    total = sum(table.values(), 0)

    ordered = sorted(table.items(), key=lambda pair: (pair[1], pair[0]), reverse=True)

    for_print = ordered[:output_limit]

    for character, count in for_print:
        if character.isspace():
            character = character.encode('unicode_escape')

        print('{0}: {1}% ({2} entries)'.format(
            character,
            count / total * 100,
            count))

    left = len(ordered) - len(for_print)

    if left > 0:
        print('and {0} more'.format(left))


def main(argv: list) -> None:
    file_name = None
    only_alpha = True
    show_bar_chart = False
    output_limit = 100

    try:
        opts, _ = getopt.getopt(argv, 'hi:al:v')
    except getopt.GetoptError:
        print('usage: python frequency_cli.py -i <inputfile> [-l <output_limit> -a]')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print('usage: python frequency_cli.py -i <inputfile> [-l <output_limit> -a -v]')
            sys.exit()
        elif opt == '-i':
            file_name = arg
        elif opt == '-a':
            only_alpha = False
        elif opt == '-l':
            output_limit = int(arg)
        elif opt == '-v':
            show_bar_chart = True

    if file_name is None:
        print('-i option expected')
        sys.exit(2)

    with open(file_name) as file:
        text = file.read()

    table = frequency.get_frequencies_table(text, only_alpha)

    print_table(table, output_limit)

    if show_bar_chart:
        print('Opening chart...')
        frequency_bar_chart.show(table)


if __name__ == '__main__':
    main(sys.argv[1:])

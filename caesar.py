ALPHABET_SIZE = 26
UPPER_BASE_CODE = ord('A')
LOWER_BASE_CODE = ord('a')


def _encrypt_character(character: str, shift: int) -> str:
    if character.isalpha():
        base_code = UPPER_BASE_CODE if character.isupper() else LOWER_BASE_CODE

        code = ord(character) - base_code

        return chr(base_code + (code+shift) % ALPHABET_SIZE)
    else:
        return character


def encrypt(message: str, shift: int) -> str:
    new_message = [_encrypt_character(c, shift) for c in message]

    return ''.join(new_message)


def _decrypt_character(character: str, shift: int) -> str:
    if character.isalpha():
        base_code = UPPER_BASE_CODE if character.isupper() else LOWER_BASE_CODE

        code = ord(character) - base_code

        return chr(base_code + (code-shift) % ALPHABET_SIZE)
    else:
        return character


def decrypt(encrypted_message: str, shift: int) -> str:
    message = [_decrypt_character(c, shift) for c in encrypted_message]

    return ''.join(message)
